const pkg = require('./package.json')
const { defineConfig } = require('tsup')

module.exports = defineConfig({
  target: 'esnext',
  clean: true,
  dts: true,
  outDir: 'dist',
  external: [...Object.keys(pkg.dependencies ?? {}), ...Object.keys(pkg.peerDependencies ?? {})],
  format: ['esm'],
  entryPoints: ['src/index.ts'],
  minifyWhitespace: true,
  minifySyntax: true,
})
