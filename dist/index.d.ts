import * as child_process from 'child_process';
import * as worker_threads from 'worker_threads';
import * as electron from 'electron';
import * as sockjs from 'sockjs';

declare abstract class ChannelConnection {
    abstract get disconnected(): boolean;
    abstract get connected(): boolean;
    abstract whenConnected(): Promise<void>;
    abstract whenDisconnected(): Promise<void>;
    abstract _sendObject(obj: any): void;
    protected abstract _close(): void;
    emitObject(obj: any): void;
    emitError(...args: any[]): void;
    close(): void;
}
interface TransformOptions {
    jsonReplacer?: (this: any, key: string, value: any) => any;
    jsonReviver?: (this: any, key: string, value: any) => any;
    stringify?: (obj: any) => string;
    parse?: (string: string) => any;
}
declare type SockJsWebSocket = sockjs.Connection;
declare type NativeWebSocket = WebSocket;
declare class WebSocketChannelConnection extends ChannelConnection {
    get disconnected(): boolean;
    get connected(): boolean;
    whenConnected(): Promise<void>;
    whenDisconnected(): Promise<void>;
    _sendObject(obj: any): void;
    protected _close(): void;
    constructor(url: string | URL, options?: TransformOptions);
    constructor(url: string | URL, protocols?: string | string[], options?: TransformOptions);
    constructor(websocket: NativeWebSocket, options?: TransformOptions);
    constructor(websocket: SockJsWebSocket, options?: TransformOptions);
}
declare type ElectronMessagePort = electron.MessagePortMain;
declare type NativeMessagePort = MessagePort | worker_threads.MessagePort;
declare type NodeWorker = worker_threads.Worker;
declare class MessageChannelConnection extends ChannelConnection {
    get disconnected(): boolean;
    get connected(): boolean;
    whenConnected(): Promise<void>;
    whenDisconnected(): Promise<void>;
    _sendObject(obj: any): void;
    protected _close(): void;
    constructor(port: NativeMessagePort | ElectronMessagePort | Promise<NativeMessagePort | ElectronMessagePort>, closeOnDisconnected?: boolean);
}
declare type ChildProcess = child_process.ChildProcess;
declare class IpcConnection extends ChannelConnection {
    get disconnected(): boolean;
    get connected(): boolean;
    whenConnected(): Promise<void>;
    whenDisconnected(): Promise<void>;
    _sendObject(obj: any): void;
    protected _close(): void;
    constructor(proc?: ChildProcess | NodeJS.Process);
}
declare class WorkerConnection extends ChannelConnection {
    get disconnected(): boolean;
    get connected(): boolean;
    whenConnected(): Promise<void>;
    whenDisconnected(): Promise<void>;
    _sendObject(obj: any): void;
    protected _close(): void;
    constructor(workerOrWorkerSelf?: SharedWorker | Worker | NodeWorker | Promise<SharedWorker | Worker | NodeWorker>, closeOnDisconnected?: boolean);
}

/**
 * An events map is an interface that maps event names to their value, which
 * represents the type of the `on` listener.
 */
interface EventsMap {
    [event: string]: any;
}
interface DefaultEventsMap {
    [event: string]: (...args: any[]) => any;
}
/**
 * Returns a union type containing all the keys of an event map.
 */
declare type EventNames<Map extends EventsMap> = keyof Map & string;
/** The tuple type representing the parameters of an event listener */
declare type EventParams<Map extends EventsMap, Ev extends EventNames<Map>> = Parameters<Map[Ev]>;
declare type EventResult<Map extends EventsMap, Ev extends EventNames<Map>> = ReturnType<Map[Ev]>;

declare type MaybePromise<T> = T | Promise<T>;
declare type ReservedEventsMap = {
    '*'(): void;
    error(err: any): void;
    disconnected(connect: ChannelConnection): void;
    connected(connect: ChannelConnection): void;
};
declare class ChannelEvent<Name extends string, Args extends any[], Data> {
    readonly name: Name;
    readonly args: Args;
    readonly data?: Promise<Data> | undefined;
    constructor(name: Name, args: Args, data?: Promise<Data> | undefined);
}
declare type ChannelListener<This, E extends EventsMap = DefaultEventsMap, N extends EventNames<E> = EventNames<E>> = (this: This, ...args: EventParams<E, N>) => any;
declare type ChannelHandler<This, E extends EventsMap = DefaultEventsMap, N extends EventNames<E> = EventNames<E>> = (this: This, ...args: EventParams<E, N>) => MaybePromise<EventResult<E, N>>;
declare type ChannelEventsMapTransform<E extends EventsMap> = {
    [K in keyof E & string]: E[K] extends (...args: any[]) => any ? (e: ChannelEvent<K, Parameters<E[K]>, ReturnType<E[K]>>) => any : () => any;
};
interface RemoteChannelEmitter<Local extends EventsMap = DefaultEventsMap, Remote extends EventsMap = DefaultEventsMap, _RegisterHandles extends Record<string, any> = {}, _Events extends EventsMap = ChannelEventsMapTransform<Omit<Local, keyof ReservedEventsMap>> & ReservedEventsMap, _Handle extends EventsMap = Omit<Local, keyof ReservedEventsMap> & {
    '*'(name: string, ...args: any[]): any;
}, _Invoke extends EventsMap = Omit<Remote, keyof ReservedEventsMap>> {
    addListener<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    on<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    once<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    removeListener<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    off<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    prependListener<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    prependOnceListener<N extends EventNames<_Events>>(name: N, listener: ChannelListener<this, _Events, N>): this;
    removeAllListeners(): this;
    removeAllListeners<N extends EventNames<_Events>>(name: N): this;
    listeners<N extends EventNames<_Events>>(name: N): ChannelListener<this, _Events, N>[];
    rawListeners<N extends EventNames<_Events>>(name: N): ChannelListener<this, _Events, N>[];
    listenerCount<N extends EventNames<_Events>>(name: N): number;
    eventNames(): keyof _Events[];
    handle<N extends EventNames<Omit<_Handle, keyof _RegisterHandles & keyof _Handle>>>(name: N, handler: ChannelHandler<this, _Handle, N>): RemoteChannelEmitter<Local, Remote, _RegisterHandles & Record<N, any>>;
    removeHandler<N extends EventNames<_Handle>>(name: N): RemoteChannelEmitter<Local, Remote, Omit<_RegisterHandles, N>>;
    getHandler<N extends EventNames<_Handle>>(name: N): ChannelHandler<this, _Handle, N> | undefined;
    handlerNames(): (keyof _Handle)[];
    removeAllHandlers(): RemoteChannelEmitter<Local, Remote>;
    invoke<N extends EventNames<_Invoke>>(name: N, ...args: EventParams<_Invoke, N>): Promise<EventResult<_Invoke, N>>;
    connect(connection: ChannelConnection): Promise<void>;
    disconnect(): Promise<void>;
    ready(): Promise<any>;
    readonly connected: boolean;
}
declare const RemoteChannelEmitter: new <Local extends EventsMap = DefaultEventsMap, Remote extends EventsMap = DefaultEventsMap>(connection?: ChannelConnection) => RemoteChannelEmitter<Local, Remote>;

export { ChannelConnection, ChannelEvent, IpcConnection, MessageChannelConnection, RemoteChannelEmitter, WebSocketChannelConnection, WorkerConnection, RemoteChannelEmitter as default };
