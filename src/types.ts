/**
 * An events map is an interface that maps event names to their value, which
 * represents the type of the `on` listener.
 */
export interface EventsMap {
  [event: string]: any
}

export interface DefaultEventsMap {
  [event: string]: (...args: any[]) => any
}

/**
 * Returns a union type containing all the keys of an event map.
 */
export declare type EventNames<Map extends EventsMap> = keyof Map & string
/** The tuple type representing the parameters of an event listener */
export declare type EventParams<Map extends EventsMap, Ev extends EventNames<Map>> = Parameters<
  Map[Ev]
>
export declare type EventResult<Map extends EventsMap, Ev extends EventNames<Map>> = ReturnType<
  Map[Ev]
>
